﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dynamicweb.Extensibility.AddIns;
using Dynamicweb.Ecommerce.Orders;
using Dynamicweb.Ecommerce.Common;
using Dynamicweb.Data;
using Dynamicweb.Logging;
using Dynamicweb.Security.UserManagement;
using Dynamicweb.Mailing;

namespace SanderPortal.AddIns
{
    [AddInLabel("OrderReportMail"), AddInDescription("Sends an email with latest months sales"), AddInIgnore(false)]
    public class OrderReportMail : Dynamicweb.Scheduling.BaseScheduledTaskAddIn
    {
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public override bool Run()
        {
            DateTime now = DateTime.Now;

            Int32 centerGroupId = 4;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var thisDate = new DateTime(now.Year, now.Month, now.Day);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            var startDateMonthName = startDate.ToString("MMMM");
            var startDateYear = startDate.ToString("yyyy");

            var startDateSQL = startDate.ToString("yyyy") + "-" + startDate.ToString("mm") + "-" + startDate.ToString("dd");
            var endDateSQL = endDate.ToString("yyyy") + "-" + endDate.ToString("mm") + "-" + endDate.ToString("dd");

            // Check if it is the last day of the month
            if (thisDate != endDate)
            {
                var centerGroup = Group.GetGroupByID(centerGroupId);
                var centerGroupUsers = centerGroup.Users;
                int centerUserCount = centerGroupUsers.Count;
                string secondaryUserName = "";

                StringBuilder orderMailHtmlBody = new StringBuilder();
                StringBuilder orderMailTextBody = new StringBuilder();

                if (centerUserCount > 0)
                {
                    foreach (var centerUser in centerGroupUsers)
                    {
                        var centerUserId = centerUser.ID;

                        string sql = "SELECT * FROM EcomOrders WHERE OrderComplete = 1 AND OrderDeleted = 0 AND OrderDate >= '" + startDateSQL + "' AND OrderDate <= '" + endDateSQL + "' AND (OrderSecondaryUserId = " + centerUserId + " OR OrderCustomerAccessUserId = " + centerUserId + ")";
                        OrderCollection orders = default(OrderCollection);
                        orders = new OrderCollection();
                        orders.Load(sql);
                        orders.Sort("OrderDate", SortOrderType.Desc);

                        if(orders.Count > 0)
                        {
                            foreach (Order enumOrder in orders)
                            {
                                var secondaryUser = new User();
                                secondaryUser.Load(centerUserId);

                                if (String.IsNullOrEmpty(secondaryUserName) || (secondaryUserName != secondaryUser.Name))
                                {
                                    secondaryUserName = secondaryUser.Name;

                                    orderMailHtmlBody.AppendLine("<br>");
                                    orderMailHtmlBody.Append(secondaryUserName);
                                    orderMailHtmlBody.AppendLine("<hr>");
                                }

                                foreach (OrderLine orderLine in enumOrder.OrderLines)
                                {
                                    orderMailHtmlBody.AppendLine(enumOrder.CompletedDate + ": " + orderLine.Quantity + " stk. " + orderLine.ProductName + " (" + orderLine.ProductNumber + ")" + "<br>");
                                }
                            }
                        }
                    }

                    Message message = new Message();
                    RecipientCollection recipientCollection = new RecipientCollection();
                    Recipient recipient = new Recipient();
                    var logger = LogManager.Current.GetLogger("SanderPortal");

                    recipient.Name = "Julie Stilling Andersen";
                    recipient.EmailAddress = "julie@sander-design.dk";
                    recipientCollection.Add(recipient);

                    message.SenderEmail = "mail@sanderportal.dk";
                    message.SenderName = "Sander Portal";
                    message.Subject = "Salgsoversigt for " + startDateMonthName + " " + startDateYear;
                    message.PlainTextBody = "Salgsoversigt fra Sander Portal - " + startDateMonthName + " " + startDateYear + Environment.NewLine + Environment.NewLine + orderMailHtmlBody.ToString().Replace(" < br>", Environment.NewLine).Replace("<hr>", "---");
                    message.IncludePlainTextBody = true;
                    message.HtmlBody = "<p><strong>Salgsoversigt fra Sander Portal - " + startDateMonthName + " " + startDateYear + "</strong><br><br>" + orderMailHtmlBody.ToString() + "</p>";
                    message.DomainUrl = "sanderportal.dk";

                    // Instanciate the MessagingHandler, which will start the process.
                    //Using the CallbackHandler is reviewed in another example.
                    var handler = new MessagingHandler(message, recipientCollection);
                    var processStarted = handler.Process();

                    //The boolean 'processStarted' indicated whether the process of preprocessing, merging and sending was started.
                    //This process is run asynchronously in multiple threads to increase performance.
                    if (processStarted)
                    {
                        logger.Log("Monthly sales mail for " + startDateMonthName + " " + startDateYear + " sent to " + recipient.EmailAddress);
                    }
                    else
                    {
                        logger.Log("Monthly sales mail for " + startDateMonthName + " " + startDateYear + " COULD NOT be sent to " + recipient.EmailAddress);
                        throw new Exception("Sending could not be started");
                    }
                }
            }
            return true;
        }
    }
}
