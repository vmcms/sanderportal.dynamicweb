﻿using System;
using System.Text;
using System.Net.Mail;
using Dynamicweb.Ecommerce.Notifications;
using Dynamicweb.Ecommerce.Products;
using Dynamicweb.Logging;
using Dynamicweb.Mailing;
using Dynamicweb.Extensibility.AddIns;
using Dynamicweb.Data;
using System.Data;

namespace SanderPortal.Modules
{
    [AddInName("Order and stock management")]

    // When order is complete, check for product quantity and send e-mail if below 10 (ten).
    [Dynamicweb.Extensibility.Notifications.Subscribe(Ecommerce.Cart.CheckoutDoneOrderIsComplete)]
    public class EcomCartCheckoutDoneOrderIsCompleteObserver : Dynamicweb.Extensibility.Notifications.NotificationSubscriber
    {
        public override void OnNotify(string notification, Dynamicweb.Extensibility.Notifications.NotificationArgs args)
        {
            Ecommerce.Cart.CheckoutDoneOrderIsCompleteArgs checkoutDoneOrderIsCompleteArgs = args as Ecommerce.Cart.CheckoutDoneOrderIsCompleteArgs;

            string salesEmail = Dynamicweb.Frontend.PageView.Current().Area.Item["ModtagerEmail"].ToString();
            string operationEmail = Dynamicweb.Frontend.PageView.Current().Area.Item["ModtagerEmail2"].ToString();
            string marketingEmail = Dynamicweb.Frontend.PageView.Current().Area.Item["MarketingEmail"].ToString();
            string aktiveringEmail = Dynamicweb.Frontend.PageView.Current().Area.Item["AktiveringEmail"].ToString();
            string sanderEmail = Dynamicweb.Frontend.PageView.Current().Area.Item["CcEmail"].ToString();

            int emailPause = Convert.ToInt32(Dynamicweb.Frontend.PageView.Current().Area.Item["EmailsPause"].ToString());

            if (!string.IsNullOrEmpty(salesEmail))
            {
                // Get all products in the order
                ProductCollection products = checkoutDoneOrderIsCompleteArgs.Order.Products;

                foreach (Product product in products)
                {
                    int productStockLimit = 0;
                    var logger = LogManager.Current.GetLogger("SanderPortal");

                    try
                    {
                        productStockLimit = Int32.Parse(product.ProductFieldValues.GetProductFieldValue("stockLimit").Value.ToString());

                        if (productStockLimit > 0)
                        {
                            // If product stock count is below the products stock limit, send an notification email
                            if (product.Stock <= productStockLimit && productStockLimit > 0)
                            //if (product.Stock <= productStockLimit && productStockLimit >= 0)
                            {
                                using (IDbConnection connection = Database.CreateConnection())
                                {
                                    IDbCommand command = connection.CreateCommand();
                                    command.CommandText = "SELECT TOP 1 * FROM DB_stockmail WHERE DB_stockmail_product = '" + product.Id + "' AND DB_stockmail_sent < DATEADD(DAY, -7, GETDATE())";
                                    //Database.AddStringParam(command, "@productID", product.Id);

                                    try
                                    {
                                        IDataReader stockMailDr = command.ExecuteReader();

                                        if (!stockMailDr.Read())
                                        {
                                            stockMailDr.Dispose();

                                            bool sendSucceded = false;
                                            string senderMail = "peter@sanderportal.dk";
                                            string senderName = "Sander Portalen";
                                            string subject = "Lav lagerstatus på produktet: " + product.Name;
                                            string htmlBody = "<p>Produktet <strong>" + product.Name + " (" + product.Number + ")</strong> har nu en lagerbeholdning p&aring; <strong>" + product.Stock + " stk.</strong></p>";
                                            string recipient = "";

                                            string activationGroup = "GROUP27";
                                            string salesGroup = "GROUP1";
                                            string operationGroup = "GROUP2";
                                            string marketingGroup = "GROUP3";

                                            foreach (Group group in product.Groups)
                                            {
                                                // If primary group is "Event materiale" (aktiveringspakker)
                                                if (GetParentEcomGroup(activationGroup, group))
                                                {
                                                    if (!String.IsNullOrEmpty(aktiveringEmail))
                                                    {
                                                        recipient = aktiveringEmail;
                                                        //recipient = "kurt@vestjyskmarketing.dk";
                                                        subject = "Aktiveringspakker: " + subject;
                                                    }
                                                }
                                                // If primary group is "Salg"
                                                if (GetParentEcomGroup(salesGroup, group))
                                                    {
                                                        if (!String.IsNullOrEmpty(salesEmail))
                                                        {
                                                            recipient = salesEmail;
                                                            subject = "Salg: " + subject;
                                                        }
                                                    }
                                                // If primary group is "Drift"
                                                if (GetParentEcomGroup(operationGroup, group))
                                                {
                                                        if (!String.IsNullOrEmpty(operationEmail))
                                                        {
                                                            recipient = operationEmail;
                                                            subject = "Drift: " + subject;
                                                        }
                                                    }
                                                // If primary group is "Marketing"
                                                if (GetParentEcomGroup(marketingGroup, group))
                                                {
                                                        if (!String.IsNullOrEmpty(marketingEmail))
                                                        {
                                                            recipient = marketingEmail;
                                                            subject = "Marketing: " + subject;
                                                        }
                                                    }
                                            }
                                            /*
                                            if (GetParentEcomGroup(product.DefaultGroup).ToString() == "GROUP1")
                                            {
                                                if(!String.IsNullOrEmpty(salesEmail))
                                                {
                                                    recipient = salesEmail;
                                                    subject = "Salg: " + subject;
                                                }
                                            }
                                            // If primary group is "Drift"
                                            if (GetParentEcomGroup(product.DefaultGroup).ToString() == "GROUP2")
                                            {
                                                if (!String.IsNullOrEmpty(operationEmail))
                                                {
                                                    recipient = operationEmail;
                                                    subject = "Drift: " + subject;
                                                }
                                            }
                                            // If primary group is "Marketing"
                                            if (GetParentEcomGroup(product.DefaultGroup).ToString() == "GROUP3")
                                            {
                                                if (!String.IsNullOrEmpty(marketingEmail))
                                                {
                                                    recipient = marketingEmail;
                                                    subject = "Marketing: " + subject;
                                                }
                                            }
                                            */

                                            if (!String.IsNullOrEmpty(recipient))
                                            {
                                                using (var mailMessage = new MailMessage())
                                                {
                                                    mailMessage.Subject = subject;
                                                    mailMessage.From = new MailAddress(senderMail, senderName);
                                                    mailMessage.To.Add(recipient);
                                                    if (!String.IsNullOrEmpty(sanderEmail))
                                                    {
                                                        mailMessage.CC.Add(sanderEmail);
                                                    }
                                                    mailMessage.Bcc.Add("kurt@vestjyskmarketing.dk");
                                                    mailMessage.IsBodyHtml = true;
                                                    mailMessage.Body = htmlBody;
                                                    mailMessage.BodyEncoding = Encoding.UTF8;
                                                    mailMessage.SubjectEncoding = Encoding.UTF8;
                                                    mailMessage.HeadersEncoding = Encoding.UTF8;

                                                    sendSucceded = EmailHandler.Send(mailMessage);
                                                }

                                                /*bool sendSucceded;
                                                string senderMail = "peter@sanderportal.dk";
                                                string senderName = "Sander Portalen";
                                                string subject = "Lav lagerstatus på produktet: " + product.Name;
                                                string htmlBody = "<p>Produktet <strong>" + product.Name + " (" + product.Number + ")</strong> har nu en lagerbeholdning p&aring; <strong>" + product.Stock + " stk.</strong></p>";

                                                using (var mailMessage = new MailMessage())
                                                {
                                                    mailMessage.Subject = subject;
                                                    mailMessage.From = new MailAddress(senderMail, senderName);
                                                    mailMessage.To.Add(salesEmail);
                                                    if (!string.IsNullOrEmpty(recipEmail2))
                                                    {
                                                        mailMessage.CC.Add(recipEmail2);
                                                    }
                                                    mailMessage.Bcc.Add("kurt@vestjyskmarketing.dk");
                                                    mailMessage.IsBodyHtml = true;
                                                    mailMessage.Body = htmlBody;
                                                    mailMessage.BodyEncoding = Encoding.UTF8;
                                                    mailMessage.SubjectEncoding = Encoding.UTF8;
                                                    mailMessage.HeadersEncoding = Encoding.UTF8;

                                                    sendSucceded = EmailHandler.Send(mailMessage);
                                                }*/

                                                if (sendSucceded)
                                                {
                                                    //init vars
                                                    DateTime sent = DateTime.Now;

                                                    IDbCommand insCommand = connection.CreateCommand();
                                                    insCommand.CommandText = "INSERT INTO DB_stockmail (DB_stockmail_product, DB_stockmail_sent) VALUES (@productID, @sent)";
                                                    Database.AddStringParam(insCommand, "@productID", product.Id);
                                                    Database.AddDateTimeParam(insCommand, "@sent", sent);

                                                    try
                                                    {
                                                        insCommand.ExecuteNonQuery();

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        logger.Log(string.Format("ERROR: Cannot insert into stockmail DB ~ {0}", ex.Message.ToString()));
                                                    }

                                                    //close command
                                                    insCommand.Dispose();

                                                    logger.Log("Notification mail sent to " + recipient + " with notifiction on low stock quantity on the product \"" + product.Id + ": " + product.Name + "\"");
                                                }
                                                else
                                                {
                                                    logger.Log("ERROR: Notification mail COULD NOT be sent to " + recipient + " with notifiction on low stock quantity on the product \"" + product.Id + ": " + product.Name + "\"");
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Log(string.Format("ERROR: Query on existing stockmail DB ~ {0} {1} {2}", ex.Message.ToString(), product.Id, product.Name));
                                    }
                                }
                            }
                        }
                    } catch(Exception ex)
                    {
                        logger.Log(string.Format("ERROR: Could not get stocklimit ~ {0} {1}", ex.Message.ToString(), productStockLimit));
                    }
                }
            }
        }


        // Find parent group
        public bool GetParentEcomGroup(string groupId, Group group)
        {
            string parent_id = group.PrimaryParentGroupId;
            bool retur = false;

            if(parent_id != groupId && group.Id != groupId)
            {
                Group parent = Group.GetGroupById(parent_id);
                if (parent != null)
                {
                    GetParentEcomGroup(groupId, parent);
                } else
                {
                    retur = false;
                }
            } else
            {
                retur = true;
            }
            return retur;
        }

        // Find current group in path of groups
        /*public bool GroupInPath(string groupId, Group group)
        {
            bool retur = false;

            GroupCollection parents = group.ParentGroups;
            foreach(Group grp in parents)
            {
                if(grp.Id == groupId)
                {
                    retur = true;
                }
            }

            return retur;
        }*/
    }
}