﻿using System;
using System.Web;
using Dynamicweb.Ecommerce.Orders;
using Dynamicweb.Logging;
using Dynamicweb.Ecommerce.Notifications;


namespace SanderPortal.Subscribers
{
    [Dynamicweb.Extensibility.Notifications.Subscribe(Ecommerce.Cart.Line.Added)]
    public class CartLineAddedNotification : Dynamicweb.Extensibility.Notifications.NotificationSubscriber
    {
        public override void OnNotify(string notification, Dynamicweb.Extensibility.Notifications.NotificationArgs args)
        {
            var logger = LogManager.Current.GetLogger("SanderPortal");

            if (args == null || !(args is Ecommerce.Cart.Line.AddedArgs))
                return;

            Ecommerce.Cart.Line.AddedArgs added = (Ecommerce.Cart.Line.AddedArgs)args;
            OrderLine orderLine = added.AddedLine;

            // Check if ondemand-product
            if (orderLine.ProductNumber != "99988")
            {
                return;
            } else {
                // Get current context
                HttpContext context = HttpContext.Current;

                if(context.Request.Files.Count > 0)
                {
                    try
                    {
                        HttpPostedFile imgFile = context.Request.Files["EcomOrderLineFieldInput_PersonaleBillede"];
                        string imgFileName = added.Cart.AutoId + "-" + imgFile.FileName;
                        string imgFileDir = "/Files/Images/OnDemand/Brugerbilleder/";

                        string imgPath = context.Server.MapPath(imgFileDir) + imgFileName;
                        imgFile.SaveAs(imgPath);

                        OrderLineFieldValue orderLineField = orderLine.get_OrderLineFieldValue("PersonaleBillede");
                        orderLineField.Value = imgFileDir + imgFileName;

                        logger.Log("PersonaleBillede: " + imgFileDir + imgFileName);
                    } catch(Exception ex)
                    {
                        logger.Log("PersonaleBillede - FEJL: " + ex.Message);
                    }
                } else
                {
                    logger.Log("PersonaleBillede - FEJL: Ingen filer vedhæftet");
                }
            }

            //logger.Log("PersonaleBillede: " + orderLineField.Value.ToString());

        }
    }
}
