﻿using Dynamicweb.Ecommerce.Products;
using Dynamicweb.Logging;
using Dynamicweb.Mailing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;


namespace SanderPortal.Subscribers
{
    [Dynamicweb.Extensibility.Notifications.Subscribe(Dynamicweb.Ecommerce.Notifications.Ecommerce.Cart.SendingConfirmationMail)]
    public class SendingConfirmationMailNotification : Dynamicweb.Extensibility.Notifications.NotificationSubscriber
    {
        public override void OnNotify(string notification, Dynamicweb.Extensibility.Notifications.NotificationArgs args)
        {
            Dynamicweb.Ecommerce.Notifications.Ecommerce.Cart.SendingConfirmationMailArgs sendingConfirmationMailArgs = args as Dynamicweb.Ecommerce.Notifications.Ecommerce.Cart.SendingConfirmationMailArgs;

            var logger = LogManager.Current.GetLogger("SanderPortal");

            ProductCollection products = sendingConfirmationMailArgs.Order.Products;

            foreach (Product product in products)
            {
                string notifyEmail = product.GetProductFieldValue("NotifyEmail").ToString();
                if (!String.IsNullOrEmpty(notifyEmail))
                {
                    MailMessage thisMailMessage = sendingConfirmationMailArgs.MailMessage;

                    thisMailMessage.To.Add(notifyEmail);
                    thisMailMessage.Bcc.Add("kurt@vestjyskmarketing.dk");

                    logger.Log("Speciel produktadvisering: Tilføjet " + notifyEmail + " som modtager af ordrebekræftelse for produktet " + product.Name);
                }
            }
        }
    }
}
