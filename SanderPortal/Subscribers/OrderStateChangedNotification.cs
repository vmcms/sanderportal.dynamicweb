﻿using System;
using System.Net;
using WebSupergoo.ABCpdf9;
using Dynamicweb.Logging;
using Dynamicweb.Ecommerce.Orders;
using Dynamicweb.Ecommerce.Products;
using Dynamicweb.Ecommerce.Notifications;
using System.Globalization;
using System.Collections.Generic;

namespace SanderPortal.Subscribers
{
    [Dynamicweb.Extensibility.Notifications.Subscribe(Ecommerce.Order.State.Changed)]
    public class OrderStateChangedNotification : Dynamicweb.Extensibility.Notifications.NotificationSubscriber
    {
        public override void OnNotify(string notification, Dynamicweb.Extensibility.Notifications.NotificationArgs args)
        {
            Ecommerce.Order.State.ChangedArgs changedArgs = args as Ecommerce.Order.State.ChangedArgs;

            var orderState = changedArgs.Order.OrderState.Name;

            if(orderState == "Generer PDF")
            {
                OrderLineCollection orderLC = new OrderLineCollection();
                orderLC = changedArgs.Order.OrderLines;

                var orderID = changedArgs.Order.Id;
                var logger = LogManager.Current.GetLogger("SanderPortal");

                foreach (OrderLine orderL in orderLC)
                {
                    if(orderL.Product.Number == "99988")
                    {
                        try {
                            ProductFieldValueCollection pfvCollection = new ProductFieldValueCollection();
                            pfvCollection = orderL.Product.ProductFieldValues;

                            int box1Left = 0;
                            int box1Top = 0;
                            int box1FontSize = 35;
                            int box1ReducedFontSize = 31;
                            int box1LineSpacing = 35;

                            int box2Left = 0;
                            int box2Top = 0;
                            int box2FontSize = 28;
                            int box2LineSpacing = 35;

                            int box3Left = 0;
                            int box3Top = 0;
                            int box3FontSize = 28;
                            int box3ReducedFontSize = 24;
                            int box3LineSpacing = 35;

                            int box4Left = 0;
                            int box4Top = 0;
                            int box4FontSize = 20;
                            int box4LineSpacing = 24;

                            int box5Left = 0;
                            int box5Top = 0;
                            int box5FontSize = 12;
                            int box5LineSpacing = 16;

                            bool dateNewLine = true;
                            bool hideDate = false;

                            string pdfTemplate = string.Empty;

                            //int box1Width = 0;
                            //int box1Height = 0;

                            foreach (var pfValue in pfvCollection)
                            {
                                var pfSystemName = pfValue.ProductField.SystemName;

                                switch (pfSystemName)
                                {
                                    case "Box1Left":
                                        box1Left = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box1Top":
                                        box1Top = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box1FontSize":
                                        box1FontSize = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box1ReducedFontSize":
                                        box1ReducedFontSize = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box1LineSpacing":
                                        box1LineSpacing = Convert.ToInt32(pfValue.Value);
                                        break;

                                    case "Box2Left":
                                        box2Left = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box2Top":
                                        box2Top = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box2FontSize":
                                        box2FontSize = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box2LineSpacing":
                                        box2LineSpacing = Convert.ToInt32(pfValue.Value);
                                        break;

                                    case "Box3Left":
                                        box3Left = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box3Top":
                                        box3Top = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box3FontSize":
                                        box3FontSize = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box3ReducedFontSize":
                                        box3ReducedFontSize = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box3LineSpacing":
                                        box3LineSpacing = Convert.ToInt32(pfValue.Value);
                                        break;

                                    case "Box4Left":
                                        box4Left = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box4Top":
                                        box4Top = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box4FontSize":
                                        box4FontSize = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box4LineSpacing":
                                        box4LineSpacing = Convert.ToInt32(pfValue.Value);
                                        break;

                                    case "Box5Left":
                                        box5Left = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box5Top":
                                        box5Top = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box5FontSize":
                                        box5FontSize = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "Box5LineSpacing":
                                        box5LineSpacing = Convert.ToInt32(pfValue.Value);
                                        break;
                                    case "PDFtemplate":
                                        pdfTemplate = pfValue.Value.ToString();
                                        break;
                                    case "HideDate":
                                        hideDate = Convert.ToBoolean(pfValue.Value);
                                        break;
                                    case "dateNewLine":
                                        dateNewLine = Convert.ToBoolean(pfValue.Value);
                                        break;
                                }
                            }

                            // Initialize the PDF-document
                            Doc theDoc = new Doc();
                            pdfTemplate = pdfTemplate.Replace("../", "/Files/");
                            theDoc.Read(Dynamicweb.Context.Current.Server.MapPath(pdfTemplate));

                            // Initialize document settings
                            theDoc.PageNumber = 1;
                            theDoc.TextStyle.Justification = 1;
                            theDoc.Color.String = "255 255 255";

                            int lightFont = theDoc.EmbedFont(Dynamicweb.Context.Current.Server.MapPath("/fonts/StainLig.ttf"), LanguageType.Latin);
                            int boldFont = theDoc.EmbedFont(Dynamicweb.Context.Current.Server.MapPath("/fonts/Stainless-Bold.ttf"), LanguageType.Latin);

                            string center = string.Empty;
                            string dateFrom = string.Empty;
                            string dateTo = string.Empty;
                            List<string> openHours = new List<string>();
                            string newsText = string.Empty;
                            string titleText = string.Empty;
                            string nameText = string.Empty;
                            string emailText = string.Empty;
                            string profileImg = string.Empty;
                            string aboutText = string.Empty;
                            string ptWhoText = string.Empty;
                            string ptWhatText = string.Empty;
                            string variantType = orderL.ProductVariantId;

                            theDoc.Font = lightFont;
                            theDoc.TopDown = true;

                            OrderLineFieldValueCollection orderLFC = new OrderLineFieldValueCollection();
                            orderLFC = orderL.OrderLineFieldValues;

                            // Initialize document settings once again :)
                            theDoc.FontSize = box1FontSize;

                            //theDoc.AddGrid();

                            // Page 1 of the PDF
                            if (box1Left > 0)
                            {
                                //theDoc.Rect.String = box1Left + " 490 380 " + box1Top;
                                //theDoc.Rect.String = "50 530 379 490";
                                //theDoc.FrameRect();

                                int centerLimit = 14;

                                foreach (OrderLineFieldValue olfv in orderLFC)
                                {
                                    if (olfv.OrderLineFieldSystemName == "Center")
                                    {
                                        center = WebUtility.HtmlDecode(olfv.Value.ToString());
                                        center = center.ToUpper();
                                    }
                                    if (olfv.OrderLineFieldSystemName == "DatoTil")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            DateTime realDateTo = DateTime.ParseExact(olfv.Value, "d/M-yyyy", CultureInfo.InvariantCulture);
                                            dateTo = String.Format("{0:d/M yyyy}", realDateTo);
                                        }
                                    }
                                    if (olfv.OrderLineFieldSystemName == "DatoFra")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            dateFrom = olfv.Value;
                                        }
                                    }
                                    if (olfv.OrderLineFieldSystemName.Substring(0, 5) == "Aaben")
                                    {
                                        openHours.Add(WebUtility.HtmlDecode(olfv.Value.ToString()));
                                    }
                                    if (olfv.OrderLineFieldSystemName == "Nyhedstekst")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            newsText = WebUtility.HtmlDecode(olfv.Value);
                                        }
                                    }
                                    if (olfv.OrderLineFieldSystemName == "PersonaleTitel")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            titleText = WebUtility.HtmlDecode(olfv.Value);
                                            titleText = titleText.ToUpper();
                                        }
                                    }
                                    if (olfv.OrderLineFieldSystemName == "PersonaleNavn")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            nameText = WebUtility.HtmlDecode(olfv.Value);
                                            nameText = nameText.ToUpper();
                                        }
                                    }
                                    if (olfv.OrderLineFieldSystemName == "PTHvemErJeg")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            ptWhoText = WebUtility.HtmlDecode(olfv.Value);
                                        }
                                    }
                                    if (olfv.OrderLineFieldSystemName == "PTHvordanKanJegHjaelpe")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            ptWhatText = WebUtility.HtmlDecode(olfv.Value);
                                        }
                                    }
                                }

                                theDoc.Pos.X = box1Left;
                                theDoc.Pos.Y = box1Top;
                                //theDoc.AddGrid();

                                // Should we print the date on the PDF or not?
                                if (!hideDate)
                                {
                                    if (dateNewLine)
                                    {
                                        if(center.Length > 0)
                                        {
                                            theDoc.Color.String = "255 255 255";

                                            if (center.Length > centerLimit)
                                            {
                                                if (box1ReducedFontSize > 0)
                                                {
                                                    theDoc.FontSize = box1ReducedFontSize;

                                                    int linespaceDiff = 0;
                                                    linespaceDiff = box1FontSize - box1ReducedFontSize;
                                                    box1LineSpacing = box1LineSpacing - linespaceDiff;
                                                }
                                                else
                                                {
                                                    theDoc.FontSize = box1FontSize;
                                                }
                                            }
                                            theDoc.AddText(center);

                                            theDoc.Pos.X = box1Left;
                                            theDoc.Pos.Y = box1Top + box1LineSpacing;

                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                dateFrom = dateFrom.Substring(0, dateFrom.Length - 5);
                                            }

                                            theDoc.AddText(dateFrom);

                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                theDoc.AddText(" - " + dateTo);
                                            }
                                        }

                                        if (openHours.Count > 0)
                                        {
                                            if(variantType == "VO84")
                                            {
                                                theDoc.PageNumber = 2;
                                            }
                                            theDoc.Color.String = "0 0 0";
                                            theDoc.Pos.Y = box1Top;
                                            foreach (string hrs in openHours)
                                            {
                                                double currentY = theDoc.Pos.Y;

                                                theDoc.Pos.X = box1Left;
                                                theDoc.Pos.Y = currentY + box1LineSpacing;
                                                theDoc.AddText(hrs);
                                            }
                                        }
                                        if(newsText.Length > 0)
                                        {
                                            int padding = 150;

                                            if (theDoc.MediaBox.Width > 1000)
                                            {
                                                padding = 250;
                                            }
                                            theDoc.Rect.String = box1Left + " " + (theDoc.MediaBox.Height - padding) + " " + (theDoc.MediaBox.Width - padding) + " " + box1Top;
                                            theDoc.TextStyle.Justification = 0;
                                            theDoc.AddText(newsText.Replace("<br />", "\r\n"));
                                            theDoc.TextStyle.Justification = 1.0;
                                        }
                                        if (ptWhoText.Length > 0)
                                        {
                                            int padding = 50;
                                            theDoc.Color.String = "255 255 255";

                                            theDoc.Rect.String = box1Left + " " + (box1Top + 120) + " " + (theDoc.MediaBox.Width - padding) + " " + box1Top;

                                            theDoc.AddText(ptWhoText);
                                        }
                                        if (ptWhatText.Length > 0)
                                        {
                                            int padding = 50;
                                            theDoc.Color.String = "255 255 255";

                                            theDoc.Rect.String = box1Left + " " + (box3Top + 130) + " " + (theDoc.MediaBox.Width - padding) + " " + box3Top;

                                            theDoc.AddText(ptWhatText);
                                        }
                                    }
                                    else
                                    {
                                        if(center.Length > 0)
                                        {
                                            theDoc.Color.String = "255 255 255";
                                            if ((center + " " + dateFrom + " - " + dateTo).Length > centerLimit)
                                            {
                                                if (box1ReducedFontSize > 0)
                                                {
                                                    theDoc.FontSize = box1ReducedFontSize;
                                                }
                                                else
                                                {
                                                    theDoc.FontSize = box1FontSize;
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                dateFrom = dateFrom.Substring(0, dateFrom.Length - 5);
                                            }

                                            theDoc.AddText(center + " " + dateFrom);

                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                theDoc.AddText(" - " + dateTo);
                                            }
                                        }

                                        if (openHours.Count > 0)
                                        {
                                            if (variantType == "VO84")
                                            {
                                                theDoc.PageNumber = 2;
                                            }

                                            theDoc.Color.String = "0 0 0";
                                            theDoc.Pos.Y = box1Top;
                                            foreach (string hrs in openHours)
                                            {
                                                double currentY = theDoc.Pos.Y;
                                                theDoc.Pos.X = box1Left;
                                                theDoc.Pos.Y = currentY + box1LineSpacing;
                                                theDoc.AddText(hrs);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if(center.Length > 0)
                                    {
                                        theDoc.Color.String = "255 255 255";
                                        if (center.Length > centerLimit)
                                        {
                                            if (box1ReducedFontSize > 0)
                                            {
                                                theDoc.FontSize = box1ReducedFontSize;
                                            }
                                            else
                                            {
                                                theDoc.FontSize = box1FontSize;
                                            }
                                        }
                                        theDoc.AddText(center);
                                    }
                                    if(openHours.Count > 0)
                                    {
                                        if (variantType == "VO84")
                                        {
                                            theDoc.PageNumber = 2;
                                        }
                                        theDoc.Color.String = "0 0 0";
                                        theDoc.Pos.Y = box1Top;
                                        foreach (string hrs in openHours)
                                        {
                                            double currentY = theDoc.Pos.Y;
                                            theDoc.Pos.X = box1Left;
                                            theDoc.Pos.Y = currentY + box1LineSpacing;
                                            theDoc.AddText(hrs);
                                        }
                                    }
                                }
                                if (newsText.Length > 0)
                                {
                                    int padding = 150;

                                    if (theDoc.MediaBox.Width > 1000)
                                    {
                                        padding = 250;
                                    }
                                    theDoc.Rect.String = box1Left + " " + (theDoc.MediaBox.Height - padding) + " " + (theDoc.MediaBox.Width - padding) + " " + box1Top;
                                    theDoc.TextStyle.Justification = 0;
                                    theDoc.AddText(newsText.Replace("<br />", "\r\n"));
                                    theDoc.TextStyle.Justification = 1.0;
                                }

                                if (titleText.Length > 0)
                                {
                                    int padding = 215;
                                    theDoc.Font = boldFont;
                                    theDoc.Color.String = "255 255 255";
                                    if (theDoc.MediaBox.Width > 1000)
                                    {
                                        padding = 250;
                                    }
                                    theDoc.Rect.String = box1Left + " " + (theDoc.MediaBox.Height - padding) + " " + (theDoc.MediaBox.Width - padding) + " " + box1Top;
                                    theDoc.AddHtml("<p align=center>" + titleText + "</p>");
                                }

                                if (ptWhoText.Length > 0)
                                {
                                    int padding = 50;
                                    theDoc.Color.String = "255 255 255";

                                    theDoc.Rect.String = box1Left + " " + (box1Top + 120) + " " + (theDoc.MediaBox.Width - padding) + " " + box1Top;

                                    theDoc.AddText(ptWhoText);
                                }
                                if (ptWhatText.Length > 0)
                                {
                                    int padding = 50;
                                    theDoc.Color.String = "255 255 255";

                                    theDoc.Rect.String = box3Left + " " + (box3Top + 130) + " " + (theDoc.MediaBox.Width - padding) + " " + box3Top;

                                    theDoc.AddText(ptWhatText);
                                }
                            }

                            if(box2Left > 0)
                            {
                                theDoc.FontSize = box2FontSize;
                                foreach (OrderLineFieldValue olfv in orderLFC)
                                {
                                    if (olfv.OrderLineFieldSystemName == "Bullet1")
                                    {
                                        theDoc.Color.String = "255 255 255";
                                        theDoc.Pos.X = box2Left;
                                        theDoc.Pos.Y = box2Top;

                                        string bullet1 = WebUtility.HtmlDecode(olfv.Value.ToString());
                                        theDoc.AddText(bullet1);
                                    }
                                    if (olfv.OrderLineFieldSystemName == "Bullet2")
                                    {
                                        theDoc.Color.String = "255 255 255";
                                        theDoc.Pos.X = box2Left;
                                        theDoc.Pos.Y = box2Top + box2LineSpacing;

                                        string bullet2 = WebUtility.HtmlDecode(olfv.Value.ToString());
                                        theDoc.AddText(bullet2);
                                    }
                                    if (olfv.OrderLineFieldSystemName == "Bullet3")
                                    {
                                        theDoc.Color.String = "255 255 255";
                                        theDoc.Pos.X = box2Left;
                                        theDoc.Pos.Y = box2Top + (box2LineSpacing * 2);

                                        string bullet3 = WebUtility.HtmlDecode(olfv.Value.ToString());
                                        theDoc.AddText(bullet3);
                                    }
                                    if (olfv.OrderLineFieldSystemName == "PersonaleBillede")
                                    {
                                        if (!string.IsNullOrEmpty(olfv.Value))
                                        {
                                            profileImg = WebUtility.HtmlDecode(olfv.Value);

                                            /*if(pdfTemplate.IndexOf("FW-infoskilt-pt-A4.pdf") > 0)
                                            {
                                                theDoc.Rect.String = box2Left + " " + (box2Top + 322) + " " + (box2Left + 239) + " " + box2Top;
                                            } else
                                            {*/

                                            if (pdfTemplate.Contains("konsultation"))
                                            {
                                                theDoc.Rect.String = box2Left + " " + (box2Top + 260) + " " + (box2Left + 224) + " " + box2Top;
                                            } else {
                                                theDoc.Rect.String = box2Left + " " + (box2Top + 301) + " " + (box2Left + 238) + " " + box2Top;
                                            }
                                            // 218 660 455 360
                                            //}

                                            if(!string.IsNullOrEmpty(profileImg))
                                            {
                                                if(profileImg.Substring(profileImg.Length - 1, 1) != "-")
                                                {
                                                    string thePath = System.Web.HttpContext.Current.Server.MapPath(profileImg);
                                                    theDoc.AddImageFile(thePath, 1);
                                                }
                                            }

                                            //Doc.AddImageObject
                                        }
                                    }
                                }
                            }

                            // Enter duty text if specified and the template has the word "gratis" in its name
                            if (pdfTemplate.IndexOf("gratis") > 0)
                            {
                                foreach (OrderLineFieldValue olfv in orderLFC)
                                {
                                    if (olfv.OrderLineFieldSystemName == "Pligttekst")
                                    {
                                        double dutyBoxLeft;
                                        double dutyBoxTop;
                                        double dutyBoxBottom;
                                        double dutyBoxRight;

                                        theDoc.FontSize = 8;
                                        theDoc.Color.String = "255 255 255";

                                        if (pdfTemplate.IndexOf("A5") > 0) {
                                            dutyBoxLeft = theDoc.MediaBox.Left + 30;
                                            dutyBoxTop = theDoc.MediaBox.Bottom - 45;
                                            dutyBoxBottom = theDoc.MediaBox.Bottom - 20;
                                            dutyBoxRight = theDoc.MediaBox.Width - 30;
                                            theDoc.FontSize = 7;
                                        }
                                        else if (pdfTemplate.IndexOf("A4") > 0)
                                        {
                                            dutyBoxLeft = theDoc.MediaBox.Left + 40;
                                            dutyBoxTop = theDoc.MediaBox.Bottom - 60;
                                            dutyBoxBottom = theDoc.MediaBox.Bottom - 35;
                                            dutyBoxRight = theDoc.MediaBox.Width - 40;
                                        } else
                                        {
                                            dutyBoxLeft = theDoc.MediaBox.Left + 50;
                                            dutyBoxTop = theDoc.MediaBox.Bottom - 70;
                                            dutyBoxBottom = theDoc.MediaBox.Bottom - 40;
                                            dutyBoxRight = theDoc.MediaBox.Width - 50;
                                        }

                                        theDoc.Rect.String = dutyBoxLeft.ToString() + " " + dutyBoxBottom.ToString() + " " + dutyBoxRight.ToString() + " " + dutyBoxTop.ToString();

                                        string dutyText = WebUtility.HtmlDecode(olfv.Value.ToString());
                                        theDoc.AddText(dutyText);

                                        theDoc.Rect.Dispose();
                                    }
                                }
                            }

                            // Page 2 of the PDF (if it exists)
                            if (box3Left > 0 || box4Left > 0 || box5Left > 0)
                            {
                                theDoc.PageNumber = 2;

                                // Enter duty text if specified and the template has the word "gratis" in its name
                                /*if (pdfTemplate.IndexOf("gratis") > 0)
                                {
                                    foreach (OrderLineFieldValue olfv in orderLFC)
                                    {
                                        if (olfv.OrderLineFieldSystemName == "Pligttekst")
                                        {
                                            theDoc.Pos.X = box1Left - 35;
                                            theDoc.Pos.Y = theDoc.MediaBox.Bottom - 45;
                                            theDoc.FontSize = 8;

                                            string dutyText = WebUtility.HtmlDecode(olfv.Value.ToString());
                                            theDoc.AddText(dutyText);
                                        }
                                    }
                                }*/

                                if (box3Left > 0)
                                {
                                    theDoc.FontSize = box3FontSize;

                                    int centerLimit = 14;
                                    theDoc.Color.String = "255 255 255";

                                    foreach (OrderLineFieldValue olfv in orderLFC)
                                    {
                                        if (olfv.OrderLineFieldSystemName == "Center")
                                        {
                                            center = WebUtility.HtmlDecode(olfv.Value.ToString());
                                            center = center.ToUpper();
                                        }
                                        if (olfv.OrderLineFieldSystemName == "DatoFra")
                                        {
                                            if (!string.IsNullOrEmpty(olfv.Value.ToString()))
                                            {
                                                dateFrom = olfv.Value;
                                            }
                                        }
                                        if (olfv.OrderLineFieldSystemName == "DatoTil")
                                        {
                                            if(!string.IsNullOrEmpty(olfv.Value.ToString()))
                                            {
                                                DateTime realDateTo = DateTime.ParseExact(olfv.Value, "d/M-yyyy", CultureInfo.InvariantCulture);
                                                dateTo = string.Format("{0:d/M yyyy}", realDateTo);
                                            }
                                        }
                                    }

                                    theDoc.Pos.X = box3Left;
                                    theDoc.Pos.Y = box3Top;

                                    // Should we print the date on the PDF or not?
                                    if (!hideDate)
                                    {
                                        theDoc.Color.String = "255 255 255";
                                        if (dateNewLine)
                                        {
                                            if (center.Length > centerLimit)
                                            {
                                                if(box3ReducedFontSize > 0)
                                                {
                                                    theDoc.FontSize = box3ReducedFontSize;

                                                    int linespaceDiff = 0;
                                                    linespaceDiff = box3FontSize - box3ReducedFontSize;
                                                    box3LineSpacing = box3LineSpacing - linespaceDiff;
                                                } else
                                                {
                                                    theDoc.FontSize = box3FontSize;
                                                }
                                            }
                                            theDoc.AddText(center);

                                            theDoc.Pos.X = box3Left;
                                            theDoc.Pos.Y = box3Top + box3LineSpacing;


                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                dateFrom = dateFrom.Substring(0, dateFrom.Length - 5);
                                            }

                                            theDoc.AddText(dateFrom);
                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                theDoc.AddText(" - " + dateTo);
                                            }
                                        }
                                        else
                                        {
                                            if ((center + " " + dateFrom + " - " + dateTo).Length > centerLimit)
                                            {
                                                if (box3ReducedFontSize > 0)
                                                {
                                                    theDoc.FontSize = box3ReducedFontSize;
                                                } else
                                                {
                                                    theDoc.FontSize = box3FontSize;
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                dateFrom = dateFrom.Substring(0, dateFrom.Length - 5);
                                            }

                                            theDoc.AddText(center + " " + dateFrom);
                                            if (!string.IsNullOrEmpty(dateTo))
                                            {
                                                theDoc.AddText(" - " + dateTo);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        theDoc.Color.String = "255 255 255";
                                        if (center.Length > centerLimit)
                                        {
                                            if (box3ReducedFontSize > 0)
                                            {
                                                theDoc.FontSize = box3ReducedFontSize;
                                            }
                                            else
                                            {
                                                theDoc.FontSize = box3FontSize;
                                            }
                                        }
                                        theDoc.AddText(center);
                                    }
                                    if (nameText.Length > 0)
                                    {
                                        int padding = 150;

                                        theDoc.Color.String = "255 255 255";
                                        theDoc.Font = boldFont;

                                        if (theDoc.MediaBox.Width > 1000)
                                        {
                                            padding = 250;
                                        }
                                        theDoc.Rect.String = box3Left + " " + (theDoc.MediaBox.Height - padding) + " " + (theDoc.MediaBox.Width - padding) + " " + box3Top;

                                        theDoc.Rect.String = "217 710 455 685";

                                        theDoc.AddHtml("<p align=center>" + nameText + "</p>");
                                    }
                                }

                                if (box4Left > 0)
                                {
                                    theDoc.PageNumber = 2;
                                    theDoc.Font = boldFont;
                                    theDoc.FontSize = box4FontSize;
                                    theDoc.Color.String = "255 255 255";

                                    foreach (OrderLineFieldValue olfv in orderLFC)
                                    {
                                        if (olfv.OrderLineFieldSystemName == "Center")
                                        {
                                            theDoc.Pos.X = box4Left;
                                            theDoc.Pos.Y = box4Top;

                                            center = WebUtility.HtmlDecode(olfv.Value.ToString());
                                            theDoc.AddText(center.ToUpper());
                                        }
                                        if (olfv.OrderLineFieldSystemName == "PersonaleEmail")
                                        {
                                            if (!string.IsNullOrEmpty(olfv.Value))
                                            {
                                                emailText = WebUtility.HtmlDecode(olfv.Value);
                                            }
                                        }
                                        if (olfv.OrderLineFieldSystemName == "PersonaleTekst")
                                        {
                                            if (!string.IsNullOrEmpty(olfv.Value))
                                            {
                                                aboutText = WebUtility.HtmlDecode(olfv.Value);
                                            }
                                        }
                                    }
                                    if (aboutText.Length > 0)
                                    {
                                        theDoc.PageNumber = 1;

                                        int padding = 95;
                                        theDoc.Color.String = "255 255 255";
                                        theDoc.Font = lightFont;

                                        if (theDoc.MediaBox.Width > 1000)
                                        {
                                            padding = 250;
                                        }
                                        theDoc.Rect.String = box4Left + " " + (theDoc.MediaBox.Height - (padding/2)) + " " + (theDoc.MediaBox.Width - padding) + " " + box4Top;
                                        theDoc.AddText(aboutText);
                                    }
                                    if (emailText.Length > 0)
                                    {
                                        theDoc.PageNumber = 1;

                                        int padding = 50;
                                        theDoc.Font = lightFont;
                                        theDoc.Color.String = "255 255 255";

                                        if (theDoc.MediaBox.Width > 1000)
                                        {
                                            padding = 250;
                                        }
                                        theDoc.Rect.String = box4Left + " " + (theDoc.MediaBox.Height - padding) + " " + (theDoc.MediaBox.Width - padding) + " " + box4Top;
                                        theDoc.AddText(emailText);
                                    }
                                }

                                if (box5Left > 0)
                                {
                                    theDoc.PageNumber = 2;
                                    theDoc.Font = lightFont;
                                    theDoc.FontSize = box5FontSize;
                                    theDoc.Color.String = "255 255 255";

                                    theDoc.Rect.String = theDoc.MediaBox.String;

                                    foreach (OrderLineFieldValue olfv in orderLFC)
                                    {
                                        if (olfv.OrderLineFieldSystemName == "Bullet1")
                                        {
                                            theDoc.Pos.X = box5Left;
                                            theDoc.Pos.Y = box5Top;

                                            string bullet1 = WebUtility.HtmlDecode(olfv.Value);
                                            theDoc.AddText(bullet1);
                                        }
                                        if (olfv.OrderLineFieldSystemName == "Bullet2")
                                        {
                                            theDoc.Pos.X = box5Left;
                                            theDoc.Pos.Y = box5Top + box5LineSpacing;

                                            string bullet2 = WebUtility.HtmlDecode(olfv.Value);
                                            theDoc.AddText(bullet2);
                                        }
                                        if (olfv.OrderLineFieldSystemName == "Bullet3")
                                        {
                                            theDoc.Pos.X = box5Left;
                                            theDoc.Pos.Y = box5Top + (box5LineSpacing * 2);

                                            string bullet3 = WebUtility.HtmlDecode(olfv.Value);
                                            theDoc.AddText(bullet3);
                                        }
                                    }
                                }

                                // Enter duty text if specified and the template has the word "gratis" in its name
                                /*if (pdfTemplate.IndexOf("gratis") > 0)
                                {
                                    foreach (OrderLineFieldValue olfv in orderLFC)
                                    {
                                        if (olfv.OrderLineFieldSystemName == "Pligttekst")
                                        {
                                            double dutyBoxLeft;
                                            double dutyBoxTop;
                                            double dutyBoxBottom;
                                            double dutyBoxRight;

                                            theDoc.FontSize = 8;

                                            if (pdfTemplate.IndexOf("A5") > 0)
                                            {
                                                dutyBoxLeft = theDoc.MediaBox.Left + 30;
                                                dutyBoxTop = theDoc.MediaBox.Bottom - 45;
                                                dutyBoxBottom = theDoc.MediaBox.Bottom - 20;
                                                dutyBoxRight = theDoc.MediaBox.Width - 30;
                                                theDoc.FontSize = 7;
                                            }
                                            else if (pdfTemplate.IndexOf("A4") > 0)
                                            {
                                                dutyBoxLeft = theDoc.MediaBox.Left + 40;
                                                dutyBoxTop = theDoc.MediaBox.Bottom - 60;
                                                dutyBoxBottom = theDoc.MediaBox.Bottom - 35;
                                                dutyBoxRight = theDoc.MediaBox.Width - 40;
                                            }
                                            else
                                            {
                                                dutyBoxLeft = theDoc.MediaBox.Left + 50;
                                                dutyBoxTop = theDoc.MediaBox.Bottom - 70;
                                                dutyBoxBottom = theDoc.MediaBox.Bottom - 40;
                                                dutyBoxRight = theDoc.MediaBox.Width - 50;
                                            }

                                            theDoc.Pos.X = dutyBoxLeft;
                                            theDoc.Pos.Y = dutyBoxTop;

                                            theDoc.Rect.String = dutyBoxLeft.ToString() + " " + dutyBoxBottom.ToString() + " " + dutyBoxRight.ToString() + " " + dutyBoxTop.ToString();

                                            string dutyText = WebUtility.HtmlDecode(olfv.Value.ToString());
                                            theDoc.AddText(dutyText);

                                            theDoc.Rect.Dispose();
                                        }
                                    }
                                }*/
                            }


                            // Save the generated PDF-file into the filearchive
                            var PDFfile = "/Files/Files/OnDemand/OnDemand-" + orderID + "-" + orderL.Id + ".pdf";

                            theDoc.Save(Dynamicweb.Context.Current.Server.MapPath(PDFfile));
                            theDoc.Clear();

                            // Adds the generated PDF as an attachment to the orderline
                            orderL.Attachment = PDFfile;
                            orderL.Save();

                            //theDoc.Rect.String = theDoc.CropBox.String;
                            //theDoc.Rendering.Save(Dynamicweb.Context.Current.Server.MapPath("/flyver.png"));

                            /*Message message = new Message();
                            RecipientCollection recipientCollection = new RecipientCollection();
                            Recipient recipient = new Recipient();

                            recipient.Name = "Kurt";
                            recipient.EmailAddress = "kurt@vestjyskmarketing.dk";
                            recipientCollection.Add(recipient);

                            message.SenderEmail = "mail@sanderportal.dk";
                            message.SenderName = "Sander Portal";
                            message.Subject = "OnDemand produkt";
                            message.PlainTextBody = "Status er ændret";
                            message.IncludePlainTextBody = true;
                            message.HtmlBody = "<p>Status er ændret til: " + orderState + "</p>";
                            message.DomainUrl = "sanderportal.dk";

                            // Instanciate the MessagingHandler, which will start the process.
                            //Using the CallbackHandler is reviewed in another example.
                            var handler = new MessagingHandler(message, recipientCollection);
                            var processStarted = handler.Process();*/
                        }
                        catch (Exception ex)
                        {
                            logger.Log(string.Format("ERROR: PDF-generation ~ {0}", ex.Message.ToString()));
                            logger.Log(ex.StackTrace);
                        }
                    }
                }
            }
        }
    }
}
