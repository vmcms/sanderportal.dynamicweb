﻿using Dynamicweb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Threading.Tasks;
using Dynamicweb.Logging;
using Dynamicweb.Ecommerce.Notifications;
using Dynamicweb.Ecommerce.Products;
using Dynamicweb.Mailing;
using Dynamicweb.Data;

namespace SanderPortal.Subscribers
{
    //[Dynamicweb.Extensibility.Notifications.Subscribe(Dynamicweb.Ecommerce.Notifications.Ecommerce.Cart.SendingConfirmationMail)]
    [Dynamicweb.Extensibility.Notifications.Subscribe(Ecommerce.Cart.CheckoutDoneOrderIsComplete)]
    public class EcomCartSendingConfirmationMailObserver : Dynamicweb.Extensibility.Notifications.NotificationSubscriber
    {
        public override void OnNotify(string notification, Dynamicweb.Extensibility.Notifications.NotificationArgs args)
        {
            Ecommerce.Cart.CheckoutDoneOrderIsCompleteArgs checkoutDoneOrderIsCompleteArgs = args as Ecommerce.Cart.CheckoutDoneOrderIsCompleteArgs;

            //Add code here
            var logger = LogManager.Current.GetLogger("SanderPortal");
            string ondemandEmail = Dynamicweb.Frontend.PageView.Current().Area.Item["OndemandEmail"].ToString();

            if (!string.IsNullOrEmpty(ondemandEmail))
            {
                // Get all products in the order
                ProductCollection products = checkoutDoneOrderIsCompleteArgs.Order.Products;

                bool isOndemand = false;

                foreach (Product product in products)
                {
                    if (product.GetProductFieldValue("Ondemand").ToString() == "True")
                    {
                        isOndemand = true;
                        break;
                    }
                }

                if (isOndemand)
                {
                    string orderId = checkoutDoneOrderIsCompleteArgs.Order.Id;
                    string senderMail = "peter@sanderportal.dk";
                    string senderName = "Sander Portalen";
                    string subject = "Ny Ondemand-ordre: " + orderId;
                    string htmlBody = "<p>Der er afgivet en ny Ondemand-ordre afgivet på Sanderportal.dk, i ordre: <strong>" + orderId + "</strong></p>";
                    bool sendSucceded = false;

                    using (var mailMessage = new MailMessage())
                    {
                        mailMessage.Subject = subject;
                        mailMessage.From = new MailAddress(senderMail, senderName);
                        mailMessage.To.Add(ondemandEmail);
                        mailMessage.Bcc.Add("kurt@vestjyskmarketing.dk");
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = htmlBody;
                        mailMessage.BodyEncoding = Encoding.UTF8;
                        mailMessage.SubjectEncoding = Encoding.UTF8;
                        mailMessage.HeadersEncoding = Encoding.UTF8;

                        sendSucceded = EmailHandler.Send(mailMessage);
                    }
                    if (sendSucceded)
                        logger.Log("Notification mail sent to " + ondemandEmail + " about new Ondemand-order: " + orderId);
                    else
                        logger.Log("ERROR: Notification mail COULD NOT be sent to " + ondemandEmail + " about new Ondemand-order: " + orderId);
                }
            }
        }
    }
}