﻿using System.Text;
using System.Net.Mail;
using Dynamicweb.Logging;
using Dynamicweb.Ecommerce.Notifications;
using Dynamicweb.Ecommerce.Products;
using Dynamicweb.Mailing;

namespace SanderPortal.Subscribers
{
    //[Dynamicweb.Extensibility.Notifications.Subscribe(Dynamicweb.Ecommerce.Notifications.Ecommerce.Cart.SendingConfirmationMail)]
    [Dynamicweb.Extensibility.Notifications.Subscribe(Ecommerce.Cart.CheckoutDoneOrderIsComplete)]
    public class EcomCartSendingConfirmationMailObserver : Dynamicweb.Extensibility.Notifications.NotificationSubscriber
    {
        public override void OnNotify(string notification, Dynamicweb.Extensibility.Notifications.NotificationArgs args)
        {
            Ecommerce.Cart.CheckoutDoneOrderIsCompleteArgs checkoutDoneOrderIsCompleteArgs = args as Ecommerce.Cart.CheckoutDoneOrderIsCompleteArgs;

            var logger = LogManager.Current.GetLogger("SanderPortal");
            string ondemandEmail = Dynamicweb.Frontend.PageView.Current().Area.Item["OndemandEmail"].ToString();

            if (!string.IsNullOrEmpty(ondemandEmail))
            {
                // Get all products in the order
                ProductCollection products = checkoutDoneOrderIsCompleteArgs.Order.Products;

                bool isOndemand = false;

                foreach (Product product in products)
                {
                    if (product.GetProductFieldValue("Ondemand").ToString() == "True")
                    {
                        isOndemand = true;
                        break;
                    }
                }

                if (isOndemand)
                {
                    string orderId = checkoutDoneOrderIsCompleteArgs.Order.Id;
                    string senderMail = "peter@sanderportal.dk";
                    string senderName = "Sander Portalen";
                    string subject = "Ny Ondemand-ordre: " + orderId;
                    string htmlBody = "<p>Der er afgivet en ny Ondemand-ordre afgivet på Sanderportal.dk, i ordre: <strong>" + orderId + "</strong></p>";
                    bool sendSucceded = false;

                    using (var mailMessage = new MailMessage())
                    {
                        mailMessage.Subject = subject;
                        mailMessage.From = new MailAddress(senderMail, senderName);
                        mailMessage.To.Add(ondemandEmail);
                        mailMessage.Bcc.Add("kurt@vestjyskmarketing.dk");
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = htmlBody;
                        mailMessage.BodyEncoding = Encoding.UTF8;
                        mailMessage.SubjectEncoding = Encoding.UTF8;
                        mailMessage.HeadersEncoding = Encoding.UTF8;

                        sendSucceded = EmailHandler.Send(mailMessage);
                    }
                    if (sendSucceded)
                        logger.Log("OnDemand-advisering: E-mail sendt til " + ondemandEmail + " om OnDemand-ordren: " + orderId);
                    else
                        logger.Log("FEJL: OnDemand-advisering: E-mail kunne IKKE SENDES til " + ondemandEmail + " om OnDemand-ordren: " + orderId);
                }
            }
        }
    }
}